class CreateGroupings < ActiveRecord::Migration
  def change
    create_table :groupings do |t|
      t.integer :user_id
      t.integer :team_id
      t.boolean :is_leader, default: false

      t.timestamps null: false
    end
  end
end
