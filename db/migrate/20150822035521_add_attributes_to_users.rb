class AddAttributesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :nickname, :string
    add_column :users, :gender_cd, :integer
    add_column :users, :review, :text
    add_column :users, :phone, :string
  end
end
