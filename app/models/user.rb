class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many :groupings
  has_many :teams, through: :groupings

  validates_presence_of :name, :nickname, :gender_cd

  has_attached_file :avatar, styles: { medium: "300x300#", thumb: "100x100#" }, url: "/images/:id/:style/:filename"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
end
