class HomeController < ApplicationController
	layout 'simple', only: [:tournaments]

	def send_contact
		ContactMailer.send_contact(params[:name],params[:email],params[:number],params[:message]).deliver
	end
end
